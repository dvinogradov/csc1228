//--Change the name of the file and the comment below.
//DecimalBinaryConversion2.java
//--Edit the following line so that it contains your information.
//Vinogradov:Dimitry:A00358584:csc122834
//Submission 02
//Decimal/Binary Conversion, Part 2

/*
 * Program runs fine, no errors, no problems
 */

import java.util.Scanner;

/**
 * This class contains a complete program for converting between non-negative
 * integers in binary form and decimal form. 
 */
public class DecimalBinaryConversion2
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);
        displayIdentificationInfo();
        displayProgramDescription();
        boolean finished = false;
        do
        {
            String binaryValue;
            int decimalValue;
            char requestedAction = getRequestedAction();
            switch (requestedAction)
            {
            case 'b': case 'B':
                binaryValue = getBinaryValueFromUser();
                decimalValue = decimalFromBinary(binaryValue);
                System.out.println("The corresponding decimal value of "
                    + "binary value " + binaryValue + " is "
                        + decimalValue + ".");
                System.out.print("Press Enter to continue ... ");
                keyboard.nextLine();
                break;
            case 'd': case 'D':
                decimalValue = getDecimalValueFromUser();
                binaryValue = binaryFromDecimal(decimalValue);
                System.out.println("The corresponding binary value of "
                    + "decimal value " + decimalValue + " is "
                    + binaryValue + ".");
                System.out.print("Press Enter to continue ... ");
                keyboard.nextLine();
                break;
            case 'q': case 'Q':
                finished = true;
                System.out.println("\nQuit option chosen."
                    + "\nProgram now terminating.");
                System.out.print("Press Enter to continue ... ");
                keyboard.nextLine();
                break;
            }
        }
        while (!finished);
    }


    /**
     * Displays identification information and pauses for user to press Enter.
     */
    private static void displayIdentificationInfo()
    {
        System.out.println("\nVinogradov:Dimitry:A00358584:csc122834");
        System.out.println("Submission 02");
        System.out.println("Decimal/Binary Conversion, Part 2");
        System.out.print("Press Enter to continue ... ");
        Scanner keyboard = new Scanner(System.in);
        keyboard.nextLine();
    }


    /**
     * Displays program description and pauses for user to press Enter.
     */
    private static void displayProgramDescription()
    {
        System.out.println("\nThis program allows the user to convert either "
            + "a binary value entered by the\nuser to its equivalent decimal "
            + "value, or a decimal value entered by the user\nto its "
            + "equivalent binary value. In either case, it then displays "
            + "both the\noriginal value and the converted value on the "
            + "standard output."

            + "\n\nThe program may convert any number of values of either "
            + "type on any given run.\nOn each pass the user must either "
            + "choose to quit, or choose what kind of value\nhe or she "
            + "wishes to convert. If a conversion option is chosen, then "
            + "a valid\nvalue of the correct type is the expected input. "
            + "The program deals only with\nnon-negative values."

            + "\n\nA binary value is valid (in our case) if it contains "
            + "sixteen or fewer binary\ndigits. A binary digit is a 0 or a 1."

            + "\n\nA decimal value is valid (in our case) if it contains only "
            + "digits in the range\n0..9, and its value lies in the range "
            + "0..65535. Note that we do not permit our\ndecimal integer "
            + "values to be preceded by a + sign."
            
            + "\n\nIf a value of either type does not satisfy the necessary "
            + "criteria, an error\nmust be reported and the value must be "
            + "ignored by the program, which simply\nasks for another value "
            + "after reporting the error. In addition, if the user\ndoes not "
            + "respond correctly when asked to choose the kind of value to "
            + "convert,\nat that point the program also reports the error "
            + "and repeats the request.");
        System.out.print("Press Enter to continue ... ");
        Scanner keyboard = new Scanner(System.in);
        keyboard.nextLine();
    }


    /**
     * Prompts the user to enter a single character to choose whether to
     * convert a binary or decimal value, or to quit the program, and then
     * returns that value. If the value entered is not one of the valid
     * single characters, an error is reported and the user is asked to
     * try again. [Run sample executable to see the required format for
     * the prompt and error message.]
     */
    private static char getRequestedAction()
    {
        boolean continueLoop = true;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("\nWhat kind of value would you like to convert?");
        System.out.print("Enter b (or B) for binary, d (or D) for decimal, "
             + "or q (or Q) to quit: ");
        String userAction = keyboard.nextLine();

        if (userAction.length() > 1 || userAction.equals("")) 
        {

            System.out.println("\nError: Invalid response.\n"
                + "Try again.");
            System.out.print("Press Enter to continue ... ");
            keyboard.nextLine();

            //recursive yo
            return getRequestedAction();

        }  
        else   
            return userAction.charAt(0);
    }


    /**
     * Gets a binary value from the user, entered from the keyboard.
     * This binary value must be valid, That is, it must contain
     * sixteen or fewer binary digits (0 or 1). If it is not valid,
     * the program outputs an error message and asks the user to
     * try again. [Run sample executable to see the required format
     * for the prompt and error message.] 
     */
    private static String getBinaryValueFromUser()
    {
        Scanner keyboard = new Scanner(System.in);
        int binaryValue = 0;
        
        System.out.print("\nEnter a binary value containing up to 16 digits: ");
        
        try 
        {
           binaryValue = keyboard.nextInt();
        }
        catch (Exception e)
        {
            System.out.println("\nError: Invalid binary value.\n"
                + "Try again.");
            System.out.print("Press Enter to continue ... ");
            keyboard.nextLine();
            keyboard.nextLine();

            //recursive yo
            return getBinaryValueFromUser();
        }

        String binaryString = String.valueOf(binaryValue);
        for (int i = 0; i <= binaryString.length() - 1; i++) {   

            if (!(binaryString.charAt(i) == '1' ||
                binaryString.charAt(i) == '0'))
            {
                System.out.println("\nError: Invalid binary value.\n"
                    + "Try again.");
                System.out.print("Press Enter to continue ... ");
                keyboard.nextLine();
                keyboard.nextLine();

                //recursive yo
                return getBinaryValueFromUser();
            }      
        }

        return binaryString;
    }


    /**
     * Takes in a binary value as input and returns the
     * corresponding decimal value.
     */
    private static int decimalFromBinary(String binaryValue)
    {
        int decimalValue = 0;

        for (int i = binaryValue.length() - 1, j = 0; i >= 0; i--,j++) {
            if (Integer.parseInt(String.valueOf(binaryValue.charAt(i))) != 0)
                decimalValue += Math.pow(2, j);
        }
    
        return decimalValue;
    }


    /**
     * Gets a decimal value from the user, entered from the keyboard.
     * This decimal value must be valid, That is, it must contain
     * only digits from the range 0..9, and as an integer value must
     * lie in the range 0..65535. If it is not valid, the program
     * outputs an error message and asks the user to try again.
     * [Run sample executable to see the required format for the
     * prompt and error message.] 
     */
    private static int getDecimalValueFromUser()
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("\nEnter a decimal integer in the "
                    + "range 0..65535: ");

        int originalNumber = 0;
        String newValueFromKeyboard = keyboard.nextLine();

        if (isInt(newValueFromKeyboard)) 
        {
            originalNumber = Integer.parseInt(newValueFromKeyboard);
            
            if (originalNumber > 65535 || originalNumber < 0)
            {

                System.out.print("\nError: Invalid decimal value."
                    + "\nTry again.\nPress Enter to continue ... ");
                keyboard.nextLine();

                //recurse
                return getDecimalValueFromUser();

            }            
        } 
        else 
        {

            System.out.print("\nError: Invalid decimal value."
                + "\nTry again.\nPress Enter to continue ... ");
            keyboard.nextLine();

            //recurse
            return getDecimalValueFromUser();

        }

        return originalNumber;
    }


    /**
     * Takes in a decimal value as input and returns the
     * corresponding binary value.
     */
    private static String binaryFromDecimal(int decimalValue)
    {
      
        return Integer.toBinaryString(decimalValue);
    }

    //checks if string is an int
    private static boolean isInt(String in)
    {
        try 
        {
            Integer.parseInt(in);
            return true;
        } 
        catch (Exception e) 
        {
            return false;
        }
    }

}

