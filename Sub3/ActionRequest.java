import java.util.Scanner;

public class ActionRequest {

	private int attemptsLeft = 3;
	private Converter converter = new Converter();

	public void readRequest() {

		Scanner kb = new Scanner(System.in);

		System.out.print("\nWhat kind of value would you like to convert?\n"
			+ "Enter b (or B) for binary, d (or D) for decimal, or q (or Q) "
			+ "to quit: ");

		//only returns a lowercase character
		char userAction = this.getRequest();

		//checks the different possiblities
		switch (userAction) {
			//decimal to binary
			case 'd':
				this.converter.readAndConvertDecimalValue();
				this.converter.displayBothValues();
				this.readRequest();
				break;
			//binary to decimal
			case 'b':
				this.converter.readAndConvertBinaryValue();
				this.converter.displayBothValues();
				this.readRequest();
				break;
			//restart
			case 'r':
				this.readRequest();
				break;
			//quits
			case 'q':
				System.out.print("\nQuit option chosen.\n"
					+ "Program now terminating.\n"
					+ "Press Enter to continue ... ");
				kb.nextLine();
				System.exit(0);
				break;
		}

	}

	public char getRequest() {

		Scanner kb = new Scanner(System.in);

		String kbValue = kb.nextLine().toLowerCase();

		// check to see if anything is out of whack
		if (kbValue.length() == 1 && !kbValue.equals("") && (kbValue.equals("q")
			|| kbValue.equals("b") || kbValue.equals("d"))) {
			this.attemptsLeft = 3;
			return kbValue.charAt(0);
		} else {

			//decrement
			if (--this.attemptsLeft != 0) {
				//print error
				System.out.print("\nError: Invalid Response.\n"
					+ "Try again. (You have " + this.attemptsLeft 
					+ (this.attemptsLeft > 1 ? " tries" : " try") 
					+ " left.)\nPress Enter to continue ... ");
					kb.nextLine();

				//restart loop
				return 'r';
			} else {

				//tried to many times
				System.out.print("\nError: Invalid Response.\n"
					+ "The quit option will now be supplied.\n"
					+ "Press Enter to continue ... ");
				kb.nextLine();

				return 'q';
			}


		}
	}

}