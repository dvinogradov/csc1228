
/* - SELF ASSESSMENT - 
Program runs great, no errors on my end
*/

import java.util.Scanner;

public class OpeningScreen {

	//kinda centers the opening info
	public void display() {

		System.out.print("\n\n\n\n\n\n\n\n\n\n"
			+ "\t\tVinogradov:Dimitry:A00358584:csc122834\n"
			+ "\t\tSubmission 03\n"
			+ "\t\tDecimal/Binary Conversion, Part 3"
			+ "\n\n\n\n\n\n\n\n\n\n\n\n");

		System.out.print("Press Enter to continue ... ");
		new Scanner(System.in).nextLine();
	}

}