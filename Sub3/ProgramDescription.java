import java.util.Scanner;

public class ProgramDescription {


      /* Displays program description */
	public void display() {

	System.out.println("\nThis program allows the user to convert either "
            + "a binary value entered by the\nuser to its equivalent decimal "
            + "value, or a decimal value entered by the user\nto its "
            + "equivalent binary value. In either case, it then displays "
            + "both the\noriginal value and the converted value on the "
            + "standard output."

            + "\n\nThe program may convert any number of values of either "
            + "type on any given run.\nOn each pass the user must either "
            + "choose to quit, or choose what kind of value\nhe or she "
            + "wishes to convert. If a conversion option is chosen, then "
            + "a valid\nvalue of the correct type is the expected input. "
            + "The program deals only with\nnon-negative values."

            + "\n\nA binary value is valid (in our case) if it contains "
            + "sixteen or fewer binary\ndigits. A binary digit is a 0 or a 1."

            + "\n\nA decimal value is valid (in our case) if it contains only "
            + "digits in the range\n0..9, and its value lies in the range "
            + "0..65535. Note that we do not permit our\ndecimal integer "
            + "values to be preceded by a + sign."
            
            + "\n\nIf a value of either type does not satisfy the necessary "
            + "criteria, an error\nmust be reported and the value must be "
            + "ignored by the program, which simply\nasks for another value "
            + "after reporting the error. In addition, if the user\ndoes not "
            + "respond correctly when asked to choose the kind of value to "
            + "convert,\nat that point the program also reports the error "
            + "and repeats the request.");
        System.out.print("Press Enter to continue ... ");
        new Scanner(System.in).nextLine();
	}

}