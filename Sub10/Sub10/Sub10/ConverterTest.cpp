//file ActionRequest.cpp
//Vinogradov:Dimitry:A00358584:csc122834

/* Self assessment
Runs great, no problems
*/

#include "utilities.h"
#include "ActionRequest.h"

using Scobey::DisplayOpeningScreen;
using Scobey::TextItems;

const TextItems TEXT(string);

int main(int argv, char* argc[]) {

    DisplayOpeningScreen("Vinogradov:Dimitry:A00358584:csc122834", 
        "Submission 10: Decimal/Binary Conversion, Part 3 (C++ version)");

    TEXT("Converter.txt").displayItem("ProgramDescription");

    ActionRequest *a = new ActionRequest();
    a->readRequest();

}

const TextItems TEXT(string file = "Converter.txt") {
    
    return TextItems(file);

}