//Converter.h
//Vinogradov:Dimitry:A00123456:csc122801
//Submission 11
//Decimal/Binary Conversion, Part 4 (C++ version)

/*
* This class is complete and works according to its specifications.
*/

/**
* A class to read in, validate and then convert binary numbers to the
* corresponding decimal numbers, or vice versa. Only zero and positive
* integer values falling within a limited range may be converted.
* The maximum decimal value is 65535, which corresponds to the value
* 1111111111111111 (sixteen 1's) in the binary representation.
*/

#include <string>

using std::string;

class Converter
{
public:
    Converter();
    /**<
     * Default constructor. Initializes private data members.
     */


    void readAndConvertBinaryValue();
    /**<
     * Reads a binary value from the user at the keyboard, checks the value
     * for validity, and (if valid) converts it to the equivalent decimal
     * representation. A binary value is valid if it contains 16 or fewer
     * binary digits (0 and/or 1). If the input value is invalid for any
     * reason, the following message is output and the user is required
     * to enter another value:
     * <pre>
     * Error: Invalid binary value.
     * Try again.
     * </pre>
     * @pre None
     * @post A valid binary value has been input by the user and
     * stored internally, in both binary and equivalent decimal form.
     */


    void readAndConvertDecimalValue();
    /**<
     * Reads a decimal value from the user at the keyboard, checks the value
     * for validity, and (if valid) converts it to the equivalent binary
     * representation. A decimal value is valid if it lies in the range
     * 0..65535. If the input value is invalid for any reason, the following
     * message is output and the user is required to enter another value:
     * <pre>
     * Error: Invalid decimal value.
     * Try again.
     * </pre>
     * @pre None
     * @post A valid decimal value has been input by the user and
     * stored internally in both decimal and equivalent binary form.
     */


    void displayBothValues();
    /**<
     * Displays both the binary and decimal representations of the
     * currently stored number. The format of the output is illustrated
	 * by either of the following, depending on the order of conversion:
	 * <pre>
	 * The corresponding decimal value of binary value 11 is 3.
	 * The corresponding binary value of decimal value 3 is 11.
	 * </pre>
     */

private:
    string binaryValue;
    int decimalValue;
    bool convertingBinary;
    bool convertingDecimal;
};
