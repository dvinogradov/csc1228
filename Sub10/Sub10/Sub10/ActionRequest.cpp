//file ActionRequest.cpp
//Vinogradov:Dimitry:A00358584:csc122834

/* Self assessment
Runs great, no problems
*/

#include <algorithm>
#include <string>
#include "ActionRequest.h"
#include "Converter.h"
#include "utilities.h"

using Scobey::Pause;
using std::endl;
using std::getline;
using std::cin;
using std::transform;
using std::string;

int attemptsLeft = 3;
Converter* c = new Converter();

void ActionRequest::readRequest() {

    cout << "\nWhat kind of value would you like to convert?\n"
            "Enter b (or B) for binary, d (or D) for decimal, or q (or Q) "
            "to quit: ";

    //only returns a lowercase character
    char userAction = this->getRequest();

    //checks the different possiblities
    switch (userAction) {
        //decimal to binary
        case 'd':
            c->readAndConvertDecimalValue();
            c->displayBothValues();
            this->readRequest();
            break;
        //binary to decimal
        case 'b':
            c->readAndConvertBinaryValue();
            c->displayBothValues();
            this->readRequest();
            break;
        //restart
        case 'r':
            this->readRequest();
            break;
        //quits
        case 'q':
            cout << "\nQuit option chosen.\n"
                    "Program now terminating." << endl;
                
            Pause();
            exit(0);
            break;
    }

}

char ActionRequest::getRequest() {
    
   string kbValue;
   getline(cin, kbValue);

   transform(kbValue.begin(), kbValue.end(), kbValue.begin(), ::tolower);

    // check to see if anything is out of whack
    if (kbValue.length() == 1 && !(kbValue == "") && (kbValue == "q"
        || kbValue == ("b") || kbValue == ("d"))) {
        
        attemptsLeft = 3;
        return kbValue[0];

    } else {

        //decrement
        if (--attemptsLeft != 0) {
            //print error
           cout << "\nError: Invalid Response.\n"
                   "Try again. (You have " << attemptsLeft 
                << (attemptsLeft > 1 ? " tries" : " try") 
                << " left.)" << endl;
                
           Pause();

            //restart loop
            return 'r';
        } else {

            //tried to many times
            cout << "\nError: Invalid Response.\n"
                 << "The quit option will now be supplied."
                 << endl;
            
            Pause();

            return 'q';
        }


    }


}