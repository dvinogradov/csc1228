//ActionRequest.h
//Vinogradov:Dimitry:A00358584:csc122834
//Submission 10
//Decimal/Binary Conversion, Part 3 (C++ version)

/*
 * This class is complete and working according to specifications.
 */

#ifndef ACTIONREQUEST_H
#define ACTIONREQUEST_H

/**
 * A class to handle user interaction when the user is choosing
 * whether to perform a decimal or binary conversion.
 */
class ActionRequest
{
public:
    void readRequest();
    /**<
     * Prompts the user for, and then reads from the user, a request
     * for action. The prompt has the following form:
     * <pre>
     * What kind of value would you like to convert?
     * Enter b (or B) for binary, d (or D) for decimal, or q (or Q) to quit:
     * </pre>
     * The input must be an uppercase or lowercase b, d or q, and
     * the user has three attempts to enter one of these values.
     * Depending on the situation when an invalid entry is
     * made, one of the following messages will be displayed:
     * <pre>
     * Error: Invalid response.
     * Try again. (You have 2 tries left.)
     * Press Enter to continue ...
     * </pre>
     * <pre>
     * Error: Invalid response.
     * Try again. (You have 1 try left.)
     * Press Enter to continue ...
     * </pre>
     * <pre>
     * Sorry, but you are out of tries.
     * The quit option will now be supplied.
     * Press Enter to continue ...
     * </pre>
     */


    char getRequest();
    /**<
     * Returns the request for action. This may be the request actually
     * entered by the user, or a program-supplied value of 'q' if the
     * user has been unable to make a valid choice in three attempts.
     */

private:
    char userRequest;

};

#endif