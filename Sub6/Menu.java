//Menu.java
//Vinogradov:Dimitry:A00358584:CSC122834
//Submission 06
//Implementing a Menu Class, Part 1
/* - SELF ASSESSMENT - 
Program runs great, no errors on my end
*/

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Menu {

	//no need for making the default constructor myself
	private String title = "Empty Menu";
	private Scanner kb = new Scanner(System.in);
	private List<String> options = new ArrayList<String>();

	   /**
    Sets (or resets) the title of an already existing menu. If that title
    exceeds 70 characters in length, the following message is output,
    followed by a pause:
    <pre>
	===== Error: Title length cannot exceed 70 characters.
	The title
	... text of title that was too long appears here ...
	was not added to the menu.
    </pre>
    @param firstLine The first line of text in the display.
    <br /><br />
    <strong>Pre-conditions:</strong><br />
    &nbsp;&nbsp;&nbsp;&nbsp;All input parameters have been initialized. <br /> 
    <strong>Post-conditions:</strong><br />
    &nbsp;&nbsp;&nbsp;&nbsp;The title of this Menu object has been set to the 
    value in menuTitle, or a message has been displayed indicating that title
    was too long and original title has been retained.
    */	
	public void setTitle(String menuTitle) {

		if (menuTitle.length() > 70) {

			System.out.print("\n===== Error: Title length cannot exceed 70"
				+ " characters.\n"
				+ "The title\n"
				+ menuTitle + "\n"
				+ "was not added to the menu.\n");

			this.pause();
			return;

		} else
			this.title = menuTitle;

	}

   /**
    Adds an option to the menu. The maximum number of options any menu can 
    have is 20, and the maximum length of any option is 70 characters.
    <br />
	If the number of options on the menu is already 20, the following 
	message is displayed, followed by a pause:
	<pre>
	===== Error: Maximum number of menu options (20) exceeded.
	The option
	... text of option that could not be added goes here
	was not added to the menu. 
	</pre>
	If the length of the option being added exceeds 70 characters, the
	following message is displayed, followed by a pause:
	<br />
	<pre>
	===== Error: Option cannot exceed 70 characters in length.
	The option
	... text of option that was too long appears here ...
	was not added to the menu.	 
	</pre>
	<br />

    @param optionText The text of the option to be added to the menu.
	<br /><br />
    <strong>Pre-conditions:</strong><br />
    &nbsp;&nbsp;&nbsp;&nbsp;This Menu object and optionText have been
    initialized. <br /> 
    <strong>Post-conditions:</strong><br /> 
    &nbsp;&nbsp;&nbsp;&nbsp;The option in optionText has been added to this
    Menu object, and given the next available option number, or a message has 
    been displayed indicating that the option was too long and the Menu object 
    remains unchanged.
    */
	public void addOption(String optionText) {

		if (this.options.size() >= 20) {

			System.out.print("\n===== Error: Maximum number of menu options "
				+ "(20)"
				+ " exceeded.\n"
				+ "The option\n"
				+ optionText + "\n"
				+ "was not added to the menu.\n");

			this.pause();
			return;

		}

		if (optionText.length() > 70) {

			System.out.print("\n===== Error: Option cannot exceed 70"
				+ " characters in length.\n"
				+ "The option\n"
				+ optionText + "\n"
				+ "was not added to the menu.\n");

			this.pause();
			return;

		}

		this.options.add(optionText);

	}

   /**
    Displays the menu title and options, with a blank line between the title 
    and the options. The menu is positioned more or less centered on the screen,
    with any "bias" toward the left and the top. If the menu currently has no 
    options, the title is displayed, then a blank line, and then this line is 
    displayed, starting at the left margin.
    <pre>
    This Menu currently has no options.
    </pre>
    These three lines are centered vertically, more or less, on an otherwise 
    blank screen.<br /><br />
    <strong>Pre-conditions:</strong><br />
    &nbsp;&nbsp;&nbsp;&nbsp;This Menu object has been initialized.<br /> 
    <strong>Post-conditions:</strong> <br />
    &nbsp;&nbsp;&nbsp;&nbsp;This Menu object has been displayed on the screen.
    */
	public void display() {

		//center title
		String spacesBeforeTitle = this.generateSpaces((80 
			- this.title.length()) / 2);

		//v-center menu
		String linesBeforeAfter = this.generateLines((25 
			- (2 + this.options.size())) / 2);


		String spacesBeforeOption = "";
		int longestTitle = 0;
		if (this.options.size() > 0) {

			for (int i = 1; i < this.options.size(); i++) {

				if (this.options.get(i).length() > 
					this.options.get(longestTitle).length())
					longestTitle = i;

			}

			spacesBeforeOption = this.generateSpaces((80 
				- (this.options.get(longestTitle).length() 
				+ (this.options.size() > 10 ? 4 : 3))) / 2);
			System.out.println(this.options.get(longestTitle));

		}
 
		System.out.print(linesBeforeAfter);		

		if (this.options.size() <= 0) {

			System.out.println(this.title + "\n");
			System.out.println("This menu currently has no options ... ");

		} else {

			System.out.println(spacesBeforeTitle + this.title + "\n");

			boolean extraSpace = this.options.size() >= 10 ? true : false;

			for (int i = 0; i < this.options.size(); i++) {

				String optionNumber = (extraSpace ? (((i + 1) < 10) ? " "  : "")
				 : "") + (i + 1) + ". ";
				System.out.println(spacesBeforeOption + optionNumber
				 + this.options.get(i));

			}

		}

		System.out.print(linesBeforeAfter);	

	}

	//makes any number of spaces
	private String generateSpaces(int number) {
		String spaces = "";
		for (int i = 1; i <= number; i++)
			spaces += " ";

		return spaces;
	}

   //makes any number of lines
	private String generateLines(int number) {
		String lines = "";
		for (int i = 1; i <= number; i++)
			lines += "\n";

		return lines;
	}
	
	//makes a pause
	private void pause() {

        System.out.print("Press Enter to continue ... ");
        this.kb.nextLine();

    }

}