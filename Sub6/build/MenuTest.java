//MenuTest.java
//Lastname:Firstname:A00123456:csc122801
//Submission 06
//Implementing a Menu Class, Part 1

/*
 * Self-assessment ...
 */

import java.util.Scanner;

/**
 * A class for test driving the Menu class.
 */
public class MenuTest
{

    private static void pause()
    {
        Scanner keyboard = new Scanner(System.in);
        System.out.print("Press Enter to continue ... ");
        keyboard.nextLine();
    }

    /**
     * Displays a message on an otherwise clear console screen,
     * and with a pause at the end of the screen.
     * @param message The message to be displayed, which must 
     * contain its own newline characters at those positions
     * where a new line is desired in the output.
     */
    private static void displayMessage(String message)
    {
        String linesAbove = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
        String linesBelow = "\n\n\n\n\n\n\n\n\n\n";
        System.out.println(linesAbove);
        System.out.println(message);
        System.out.println(linesBelow);
        pause();
    }

    /**
     * The main function for the test driver.
     */
    public static void main(String[] args)
    {
        OpeningScreen openingScreen = 
            new OpeningScreen("Lastname:Firstname:A00123456:csc122801",
                              "Submission 06",
                              "Implementing a Menu Class, Part 1");
        openingScreen.display();

        displayMessage("First, a default constructor creates a "
            + "\"blank\" menu, which we then display\n(on "
            + "the following screen) to show what a menu "
            + "with no title and no options\n(i.e., a "
            + "\"default menu\") looks like when displayed.\n");
        Menu menu1 = new Menu();
        menu1.display();
        pause();

        displayMessage("Next we show the error we get if we try "
            + "to add to an existing menu\na title that's too long.");
        String longTitle = new String("A Title That is Way, Way, Way, "
            + "Way, Way, Way Too Long for Any Reasonable Menu");
        menu1.setTitle(longTitle);
 

        displayMessage("And now we display the menu again to show "
            + "that it remains unchanged.");
        menu1.display();
        pause();


        displayMessage("Now we add an OK title to the default menu "
            + "and then re-display the menu.");
        menu1.setTitle("A Brand New Menu");
        menu1.display();
        pause();


        displayMessage("Now we declare a second menu object and set "
            + "its title.\nThen we add several options, displaying "
            + "the menu after adding each one.\nNotice how the menu "
            + "display (the title and options) remains \"centered\"."
            + "\nAnd note as well what happens when we try to add "
            + "an option that is too long.");
        Menu menu2 = new Menu();
        menu2.setTitle("Main Menu");
        menu2.display();
        pause();
        menu2.addOption("Quit");
        menu2.display();
        pause();
        menu2.addOption("Get information");
        menu2.display();
        pause();
        menu2.addOption("Do something");
        menu2.display();
        pause();
        menu2.addOption("Do something really, really "
            + "very long and involved");
        menu2.display();
        pause();
        String longOption = 
            new String("This option is sure going to be too "
                + "long to be added to any menu, for sure!");
        menu2.addOption(longOption);
        menu2.display();
        pause();
        menu2.addOption("Do something shorter");
        menu2.display();
        pause();
        menu2.addOption("Do something else altogether");
        menu2.display();
        pause();


        displayMessage("Create a third menu with 20 options (the maximum "
            + "number). Note how the periods\nfollowing the option numbers "
            + "line up, and continue to line up when the option\nnumber goes "
            + "from one digit to two digits.");
        Menu menu3 = new Menu();
        menu2.setTitle("Test Menu");
        menu3.addOption("Option 1 permits you to do a first thing, "
            + "which is to quit");
        menu3.addOption("Option 2 allows you to do this");
        menu3.addOption("Option 3 lets you do something just "
            + "a bit longer");
        menu3.addOption("Option 4 just lets you do it");
        menu3.addOption("Option 5 lets you do that");
        menu3.addOption("Option 6 lets you do it now, not later, "
            + "if you please");
        menu3.addOption("Option 7 lets you do whatever");
        menu3.addOption("Option 8 requires you to do it quickly "
            + "if you can");
        menu3.addOption("Option 9 lets you do it and take your time");
        menu3.addOption("Option 10 lets you do it over and over again");
        menu3.addOption("Option 11 lets you do it with style");
        menu3.addOption("Option 12 requires it to be done by Monday");
        menu3.addOption("Option 13 asks that you do it next week");
        menu3.addOption("Option 14 demands that it be done yesterday");
        menu3.addOption("Option 15 doesn't care if it's done at all");
        menu3.addOption("Option 16 lets you do it whenever you like");
        menu3.addOption("Option 17 requires it to happend every Friday");
        menu3.addOption("Option 18 suggests you do it Tuesdays "
            + "and Thursdays");
        menu3.addOption("Option 19 asks for a full report in the morning");
        menu3.addOption("Option 20 asks you to undo it");
        menu3.display();
        pause();

        displayMessage("And now note what happens when you attempt to add "
            + "another option\nto a menu that already contains the maximum "
            + "20 options.");
        menu3.addOption("And yet another option ... ");


        displayMessage("We display the menu again to show that it remains "
            + "unchanged.");
        menu3.display();
        pause();

        displayMessage("Finally, we create a fifth, rather bizarre menu "
            + "as one more example\nto show that its display is at least "
            + "reasonable.");
        String commonText = new String("Option ");
        String newOption;
        Menu menu4 = new Menu();
        menu4.setTitle("A Final Test Menu with a Very, "
            + "Very, Very, Very Long Title");
        for (int i=1; i<=19; i++)
        {
            newOption = commonText + new Integer(i).toString();
            menu4.addOption(newOption);
        }
        menu4.display();
        pause();
        menu4.addOption("And this of course is a very, very, "
            + "very long option");
        menu4.display();
        pause();

        System.out.println("\nNo further tests to perform."
            + "\nProgram will now terminate.");
        pause();
    }
}

