/* Converter.class */
import java.util.Scanner;

public class Converter {

    private boolean isBinaryConvertedLast = false;
    private String binaryValue = "0";
    private int decimalValue = 0;

    public void displayBothValues() {

        Scanner kb = new Scanner(System.in);

        if (this.isBinaryConvertedLast) {
           
            System.out.println("The corresponding decimal value of "
            + "binary value " + this.binaryValue + " is "
            + this.decimalValue + ".");

            System.out.print("Press Enter to continue ... ");
            kb.nextLine();
            
        } else {
           
            System.out.println("The corresponding binary value of "
            + "decimal value " + this.decimalValue + " is "
            + this.binaryValue + ".");

            System.out.print("Press Enter to continue ... ");
            kb.nextLine();

        }

    }

    public void readAndConvertBinaryValue() {

        Scanner kb = new Scanner(System.in);

        String binaryValue = "0";
        long binaryLong = 0;
        
        System.out.print("\nEnter a binary value containing up to 16 digits: ");
        
        try 
        {
           binaryValue = kb.nextLine();

           binaryLong = Long.parseLong(binaryValue);
        }
        catch (Exception e)
        {
            System.out.println("\nError: Invalid binary value.\n"
                + "Try again.");
            System.out.print("Press Enter to continue ... ");
            kb.nextLine();

            this.readAndConvertBinaryValue();
            return;
        }

        String binaryString = String.valueOf(binaryLong);
        for (int i = 0; i <= binaryString.length() - 1; i++) {   

            if (!(binaryString.charAt(i) == '1' ||
                binaryString.charAt(i) == '0'))
            {
                System.out.println("\nError: Invalid binary value.\n"
                    + "Try again.");
                System.out.print("Press Enter to continue ... ");
                kb.nextLine();

                this.readAndConvertBinaryValue();
                return;
            }      
        }


        int decimal = 0;
        for (int i = binaryString.length() - 1, j = 0; i >= 0; i--,j++) {
            if (Integer.parseInt(String.valueOf(binaryString.charAt(i))) != 0)
                decimal += Math.pow(2, j);
        }

        this.binaryValue = binaryString;
        this.decimalValue = decimal;
        this.isBinaryConvertedLast = true;

    }

    public void readAndConvertDecimalValue() {

        Scanner kb = new Scanner(System.in);

        System.out.print("\nEnter a decimal integer in the "
                    + "range 0..65535: ");

        int originalNumber = 0;
        String newValueFromKeyboard = kb.nextLine();

   
        try {
            originalNumber = Integer.parseInt(newValueFromKeyboard);
            
            if (originalNumber > 65535 || originalNumber < 0)
            {

                System.out.print("\nError: Invalid decimal value."
                    + "\nTry again.\nPress Enter to continue ... ");
                kb.nextLine();

                //recurse
                this.readAndConvertDecimalValue();
                return;
            }

        } catch (Exception e) {

            System.out.print("\nError: Invalid decimal value."
                + "\nTry again.\nPress Enter to continue ... ");
            kb.nextLine();

            //recurse
            this.readAndConvertDecimalValue();
            return;

        }   

        this.binaryValue = Integer.toBinaryString(originalNumber);
        this.decimalValue = originalNumber;
        this.isBinaryConvertedLast = false;

    }

}