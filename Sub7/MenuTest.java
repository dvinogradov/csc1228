//MenuTest.java
//Lastname:Firstname:A00358584:csc122834
//Submission 07
//Implementing a Menu Class, Part 2

/*
 * runs great, no problems
 */

import java.util.Scanner;

/**
 * A class for test driving the Menu class.
 */
public class MenuTest
{
    private static Scanner keyboard = new Scanner(System.in);

    private static void pause()
    {
        System.out.print("Press Enter to continue ... ");
        keyboard.nextLine();
    }

    /**
     * Displays a message on an otherwise clear console screen,
     * and with a pause at the end of the screen.
     * @param message The message to be displayed, which must 
     * contain its own newline characters at those positions
     * where a new line is desired in the output.
     */
    private static void displayMessage(String message)
    {
        String linesAbove = "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
        String linesBelow = "\n\n\n\n\n\n\n\n\n\n";
        System.out.println(linesAbove);
        System.out.println(message);
        System.out.println(linesBelow);
        pause();
    }

    /**
     * The main function for the test driver.
     */
    public static void main(String[] args)
    {
        OpeningScreen openingScreen = 
            new OpeningScreen("Vinogradov:Dimitry:A00358584:csc122834",
                              "Submission 07",
                              "Implementing a Menu Class, Part 2");
        openingScreen.display();

        displayMessage("First, a default constructor creates a "
            + "\"blank\" menu, which we then display\n(on "
            + "the following screen) to show what a menu "
            + "with no title and no options\n(i.e., a "
            + "\"default menu\") looks like when displayed.\n");
        Menu menu1 = new Menu();
        menu1.display();
        pause();

        displayMessage("Next we show the error we get if we try "
            + "to add to an existing menu\na title that's too long.");
        String longTitle = new String("A Title That is Way, Way, Way, "
            + "Way, Way, Way Too Long for Any Reasonable Menu");
        menu1.setTitle(longTitle);

        displayMessage("To confirm that the menu hasn't changed, we "
            + "display it again.");
        menu1.display();
        pause();

        displayMessage("Now add a title to the default menu and then "
            + "re-display the menu.");
        menu1.setTitle("A Brand New Menu");
        menu1.display();
        pause();

        displayMessage("Observe what happens when we attempt to get a "
            + "menu choice\nfrom a \"title-only\" menu with no options.");
        int menuChoice = menu1.getChoice();
        System.out.println("\nThe menu choice returned was "
            + menuChoice + ".");
        pause();


        displayMessage("Now we declare a second menu object and set "
            + "its title.\nThen we add several options, displaying "
            + "the menu after adding each one.\nNotice how the menu "
            + "display (the title and options) remains \"centered\"."
            + "\nAnd note as well what happens when we try to add "
            + "an option that is too long.");
        Menu menu2 = new Menu("Main Menu");
        menu2.display();
        pause();

        menu2.addOption("Quit");
        menu2.display();
        pause();

        menu2.addOption("Get information");
        menu2.display();
        pause();

        menu2.addOption("Do something");
        menu2.display();
        pause();

        menu2.addOption("Do something really, really "
            + "very long and involved");
        menu2.display();
        pause();

        String longOption = 
            new String("This option is sure going to be too long "
                + "to be added to any menu, for sure!");
        menu2.addOption(longOption);

        menu2.addOption("Do something shorter");
        menu2.display();
        pause();

        menu2.addOption("Do something else altogether");
        menu2.display();
        pause();

        displayMessage("Loop while the user does not choose the "
               + "Quit option, i.e.\nwhile the user does not choose "
               + "option 1 from the menu.");
        boolean finished;
        do
        {
            menu2.display();
            menuChoice = menu2.getChoice();
            finished = (menuChoice == 1  ||  menuChoice == -1);
            System.out.println("\nThe menu choice returned was "
                + menuChoice + ".");
            if (menuChoice != 1  &&  menuChoice != -1)
                System.out.println("We are just printing the menu "
                    + "choice here, but we could be doing "
                    + "most anything.");
            pause();
        }
        while (!finished);


        displayMessage("Create a third menu with 20 options (the maximum "
            + "number). Note how the periods\nfollowing the option numbers "
            + "line up, and continue to line up when the option\nnumber goes "
            + "from one digit to two digits.");
        Menu menu3 = new Menu("Test Menu");
        menu3.addOption("Option 1 permits you to do a first thing, "
            + "which is to quit");
        menu3.addOption("Option 2 allows you to do this");
        menu3.addOption("Option 3 lets you do something just "
            + "a bit longer");
        menu3.addOption("Option 4 just lets you do it");
        menu3.addOption("Option 5 lets you do that");
        menu3.addOption("Option 6 lets you do it now, not later, "
            + "if you please");
        menu3.addOption("Option 7 lets you do whatever");
        menu3.addOption("Option 8 requires you to do it quickly "
            + "if you can");
        menu3.addOption("Option 9 lets you do it and take your time");
        menu3.addOption("Option 10 lets you do it over and over again");
        menu3.addOption("Option 11 lets you do it with style");
        menu3.addOption("Option 12 requires it to be done by Monday");
        menu3.addOption("Option 13 asks that you do it next week");
        menu3.addOption("Option 14 demands that it be done yesterday");
        menu3.addOption("Option 15 doesn't care if it's done at all");
        menu3.addOption("Option 16 lets you do it whenever you like");
        menu3.addOption("Option 17 requires it to happend every Friday");
        menu3.addOption("Option 18 suggests you do it Tuesdays "
            + "and Thursdays");
        menu3.addOption("Option 19 asks for a full report in the morning");
        menu3.addOption("Option 20 asks you to undo it");
        menu3.display();
        pause();

        displayMessage("Note what happens when you attempt to add "
            + "another option to a\nmenu that already contains the maximum "
            + "20 options.");
        menu3.addOption("And yet another option ... ");

        displayMessage("We display the menu again to show that it remains "
            + "unchanged.");
        menu3.display();
        pause();


        displayMessage("Loop while the user does not choose the Quit "
                + "option, i.e. while the user\ndoes not choose option 1 "
                + "from the menu. This loop allows the testing of\noption "
                + "values up to and including the maximum.");
        do
        {
            menu3.display();
            menuChoice = menu3.getChoice();
            finished = (menuChoice == 1  ||  menuChoice == -1);
            System.out.println("\nThe menu choice returned was "
                + menuChoice + ".");
            pause();
        }
        while (!finished);

        displayMessage("Create a fourth menu, again with 20 options "
            + "and, this time, a very long title.\nNote the position "
            + "of that title on the line. Note as well, in passing, how "
            + "the\noptions are created and added. This is not very useful, "
            + "of course, since it is\nonly possible with options of this "
            + "form.");
        Menu menu4 = new Menu("Another Test Menu with a Very, Very, "
            + "Very, Very Long Title");

        String commonText = new String("Option ");
        String newOption;
        for (int i=1; i<=20; i++)
        {
            newOption = commonText + new Integer(i).toString();
            menu4.addOption(newOption);
        }
        menu4.display();
        pause();


        displayMessage("Create a fifth, rather bizarre menu as one more "
            + "example to show that its\ndisplay is at least reasonable.");
        Menu menu5 = new Menu("A Final Test Menu with a Very, "
            + "Very, Very, Very Long Title");
        for (int i=1; i<=19; i++)
        {
            newOption = commonText + new Integer(i).toString();
            menu5.addOption(newOption);
        }
        menu5.display();
        pause();
        menu5.addOption("And this of course is a very, very, "
            + "very long option");
        menu5.display();
        pause();

        displayMessage("Now we test the one-parameter constructor, first by "
            + "\ntrying to create a menu with a title that is too long.");
        Menu menu6 = new Menu("Title Which Is a Lot Longer Than Any That "
            + "Can Be Used for Any of Our Menus");

        displayMessage("And here is the menu, to show that it does have "
            + "that title ... ");
        menu6.display();
        pause();


        displayMessage("Now we create a menu with a title and three options."
            + "\nThe we use the default getChoice() method to get a user "
            + "option from that menu.");

        Menu menu = new Menu("A New Menu");
        menu.addOption("Quit");
        menu.addOption("Get information");
        menu.addOption("Do something");
        menu.display();

        do
        {
            menu.display();
            menuChoice = menu.getChoice();
            switch (menuChoice)
            {
            case -1:
            case 1:
                System.out.println("\nNow quitting this menu.");
                pause();
                break;
            case 2:
                System.out.println("\nGetting information ... ");
                pause();
                break;
            case 3:
                System.out.println("\nDoing something ... ");
                pause();
                break;
            }
        }
        while (menuChoice != -1  &&  menuChoice != 1);


        displayMessage("Next we use the same menu, but the version of "
            + "getChoice()\nthat takes a prompt and maximum number of "
            + "tries as input.");
        System.out.println("So ... enter the prompt you'd like to use: ");
        String prompt = keyboard.nextLine();
        System.out.print("And now your maximum number of tries: ");
        int maxNumberOfTries = keyboard.nextInt();
        keyboard.nextLine(); //to clear the input stream

        do
        {
            menu.display();
            menuChoice = menu.getChoice(prompt, maxNumberOfTries);
            switch (menuChoice)
            {
            case -1:
            case 1:
                System.out.println("\nNow quitting this menu.");
                pause();
                break;
            case 2:
                System.out.println("\nGetting information ... ");
                pause();
                break;
            case 3:
                System.out.println("\nDoing something ... ");
                pause();
                break;
            }
        }
        while (menuChoice != -1  &&  menuChoice != 1);


        displayMessage("Again we use the same menu, but the version of "
            + "getChoice() that\ntakes just a prompt and uses the default "
            + "maximum number of tries.");
        System.out.println("So ... enter the prompt you'd like to use: ");
        prompt = keyboard.nextLine();

        do
        {
            menu.display();
            menuChoice = menu.getChoice(prompt);
            switch (menuChoice)
            {
            case -1:
            case 1:
                System.out.println("\nNow quitting this menu.");
                pause();
                break;
            case 2:
                System.out.println("\nGetting information ... ");
                pause();
                break;
            case 3:
                System.out.println("\nDoing something ... ");
                pause();
                break;
            }
        }
        while (menuChoice != -1  &&  menuChoice != 1);



        displayMessage("Finally we use the same menu once more, but the "
            + "version of getChoice() that\ntakes just a maximum number "
            + "of tries as input and uses the default prompt.");
        System.out.print("So ... enter the maximum number of tries you'd "
            + "like to use: ");
        maxNumberOfTries = keyboard.nextInt();
        keyboard.nextLine(); //to clear the input stream

        do
        {
            menu.display();
            menuChoice = menu.getChoice(maxNumberOfTries);
            switch (menuChoice)
            {
            case -1:
            case 1:
                System.out.println("\nNo more tests to perform."
                    + "\nProgram now terminating.");
                pause();
                break;
            case 2:
                System.out.println("\nGetting information ... ");
                pause();
                break;
            case 3:
                System.out.println("\nDoing something ... ");
                pause();
                break;
            }
        }
        while (menuChoice != -1  &&  menuChoice != 1);
    }
}

