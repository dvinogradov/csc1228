//Menu.java
//Vinogradov:Dimitry:A00358584:CSC122834

/*A menu class for console programs. Menus may have up to 20 options and each
 option may have up to 70 characters. The menu title may also have up to 70 
 characters. Menus are displayed more or less "centered" on the screen.*/

import java.util.Scanner;
import java.util.List;
import java.util.ArrayList;

public class Menu 
{

	private String title = "Empty Menu";
	private Scanner kb = new Scanner(System.in);
	private List<String> options = new ArrayList<String>();
	private int numberOfTries = -1;


	/**
	Default constructor. Constructs a menu with zero options and a title of
	Empty Menu.
	@pre.c None.
	@post.c A Menu object has been constructed with the title "Empty Menu" 
	and no options
	*/
	public Menu() 
	{
		this("Empty Menu");
	}


	/**
	Constructor with title. Constructs a menu with zero options and a title 
	given by the input parameter. If that title exceeds 70 characters in length,
	the following message is output, followed by a pause, and the input title is
	overridden by the default title of Empty Menu:
	<pre>
	===== Error: Title cannot exceed 70 characters.
	The title
	... title that was too long goes here ...
	was not added to the menu.
	This menu has been given the default title: Empty Menu
	</pre>
	@param menuTitle The title to be given to the menu
	@pre.c None.
	@post.c A Menu object has been constructed with a title a no options.
	*/
	public Menu(String menuTitle)
	{

		if (menuTitle.length() > 70) 
		{
		
			this.title = "Empty Menu";
			
			System.out.println("\n===== Error: Title cannot exceed 70 characte"
				+ "rs.\nThe title\n" + menuTitle + "\nwas not added to the menu"
				+ ".\nThis menu has been given the default title: Empty Menu");

			this.pause();


		} else
			this.title = menuTitle;

		this.options = new ArrayList<String>();

	}

	/**
	Same as the two-parameter version, except it uses a default prompt and
	a default maximum number of tries. The default prompt is
	<pre>
	Enter the number of your menu choice here and then press Enter:
	</pre>
	@pre.c None.
	@post.c A valid menu choice has been returned, or one or more error 
	messaqes have been displayed and a value of -1 has been returned.
	*/
	public int getChoice() 
	{
		return this.getChoice("Enter the number of your menu choice here and "
			+ "then press Enter: ", 3);
	}

	/**
	Same as the two-parameter version, except it uses the default prompt and
	the maximum number of tries input.
	@param maxNumberOfTries The maximum number of tries the user has to enter 
	a valid menu choice.
	@pre.c maxNumberOfTries has been initialized.
	@post.c A valid menu choice has been returned, or one or more error messaqes
	have been displayed and a value of -1 has been returned.
	*/
	public int getChoice(int maxNumberOfTries) 
	{
		return this.getChoice("Enter the number of your menu choice here and "
			+ "then press Enter: ", maxNumberOfTries);
	}

	/**
	Same as the two-parameter version, except it uses the input prompt and the 
	default maximum number of tries, which is 3.
	@param prompt The prompt used to ask the user for a menu choice
	@pre.c prompt has been initialized.
	@post.c A valid menu choice has been returned, or one or more error messaqes
	have been displayed and a value of -1 has been returned.
	*/
	public int getChoice(String prompt) 
	{
		return this.getChoice(prompt, 3);
	}


	/**
	Prompts the user to enter a menu choice and then reads the choice entered
	by the user at the keyboard. If this method is called for a menu that has 
	zero options, the following message is displayed, and the method 
	returns a value of -1:
	<pre>
	===== Error: Menu has no options from which to choose.
	</pre>
	If the user enters an invalid menu choice, the following message is 
	displayed, and the user gets to make another choice, unless the maximum 
	number of tries allowed has been reached:
	<pre>
	Error: Menu option choice invalid.
	Choice must be a value from 1 to #. Try again.
	</pre>
	Here # represents the maximum option number on the current menu. If the 
	user makes the "maximum number of tries" invalid choices, the following 
	message is displayed, and the method returns a value of -1:
	<pre>
	Sorry, but that last choice was also invalid, and you only get # chances.
	</pre>
	Here # represents the maximum number of tries the user is permitted to enter
	a valid menu option. 
	If the user enters an empty string as a prompt, the prompt is set to
	<pre>
	Enter choice:
	</pre>
	and if the user enters a value < 2 for the maximum number of permitted 
	tries, that value is set to 2.
	@param prompt The prompt used to ask the user for a menu choice.
	@param maxNumberOfTries The maximum number of tries the user has to enter a 
	valid menu choice.
	@pre.c prompt and maxNumberOfTries are both initialized.
	@post.c A valid menu choice has been returned, or one or more error messages
	have been displayed and a value of -1 has been returned.
	*/
	public int getChoice(String prompt, int maxNumberOfTries) 
	{

		int choice = -1;
		int menuSize = this.options.size();

		if (this.numberOfTries == -1 && maxNumberOfTries < 2)
			maxNumberOfTries = this.numberOfTries = 2;
		else if (this.numberOfTries == -1)
			this.numberOfTries = maxNumberOfTries;

		prompt = (prompt.length() == 0) ? "Enter choice: " : prompt;

		//test if menu is empty
		if (this.options.size() <= 0) {

			System.out.print("\n===== Error: Menu has no options from which to "
				+ "choose.\n");

			this.pause();

			this.numberOfTries = -1;

			return -1;

		}

		//center the prompt
		String spacesToCenterPrompt = this.generateSpaces((80 
				- (1 + prompt.length())) / 2);

		//start asking questions
		System.out.print(spacesToCenterPrompt + prompt);

		try 
		{

			choice = Integer.parseInt(this.kb.nextLine());

		} catch (Exception e) 
		{

			if (--maxNumberOfTries == 0) 
			{

				System.out.println("\nSorry, but that last choice was also "
						 + "invalid, and you only get " + this.numberOfTries 
						 + " chances.");

			 	this.pause();
	
			 	this.numberOfTries = -1;

	 			return -1;

			}

			System.out.print("\nError: Menu option choice invalid.\n"
				+ "Choice must be a value from 1 to " + menuSize
				+ ". Try again.\n");

			this.pause();
			System.out.println();

			return this.getChoice(prompt, maxNumberOfTries);

		}

		//test if within range
		if (choice < 1 || choice > menuSize) 
		{

			if (--maxNumberOfTries == 0) 
			{
	
				System.out.println("\nSorry, but that last choice was also "
				 + "invalid, and you only get " + this.numberOfTries 
				 + " chances.");

			 	this.pause();

			 	this.numberOfTries = -1;
	
	 			return -1;

			}

			System.out.print("\nError: Menu option choice invalid.\n"
				+ "Choice must be a value from 1 to " + menuSize
				+ ". Try again.\n");

			this.pause();
			System.out.println();

			return this.getChoice(prompt, maxNumberOfTries);

		}

		if (choice == 1)
			this.numberOfTries = -1;

		//if there has been no hiccups, go ahead and return
		return choice;

	}


	   /**
    Sets (or resets) the title of an already existing menu. If that title
    exceeds 70 characters in length, the following message is output,
    followed by a pause:
    <pre>
	===== Error: Title length cannot exceed 70 characters.
	The title
	... text of title that was too long appears here ...
	was not added to the menu.
    </pre>
    @param firstLine The first line of text in the display.
    @pre.c All input parameters have been initialized.
    @post.c The title of this Menu object has been set to the 
    value in menuTitle, or a message has been displayed indicating that title
    was too long and original title has been retained.
    */	
	public void setTitle(String menuTitle) 
	{

		if (menuTitle.length() > 70) 
		{

			System.out.print("\n===== Error: Title length cannot exceed 70"
				+ " characters.\n"
				+ "The title\n"
				+ menuTitle + "\n"
				+ "was not added to the menu.\n");

			this.pause();
			return;

		} else
			this.title = menuTitle;

	}

   /**
    Adds an option to the menu. The maximum number of options any menu can 
    have is 20, and the maximum length of any option is 70 characters.
    <br />
	If the number of options on the menu is already 20, the following 
	message is displayed, followed by a pause:
	<pre>
	===== Error: Maximum number of menu options (20) exceeded.
	The option
	... text of option that could not be added goes here
	was not added to the menu. 
	</pre>
	If the length of the option being added exceeds 70 characters, the
	following message is displayed, followed by a pause:
	<br />
	<pre>
	===== Error: Option cannot exceed 70 characters in length.
	The option
	... text of option that was too long appears here ...
	was not added to the menu.	 
	</pre>
    @param optionText The text of the option to be added to the menu.
    @pre.c This Menu object and optionText have been
    initialized.
    @post.c The option in optionText has been added to this
    Menu object, and given the next available option number, or a message has 
    been displayed indicating that the option was too long and the Menu object 
    remains unchanged.
    */
	public void addOption(String optionText) 
	{

		if (this.options.size() >= 20) 
		{

			System.out.print("\n===== Error: Maximum number of menu options "
				+ "(20)"
				+ " exceeded.\n"
				+ "The option\n"
				+ optionText + "\n"
				+ "was not added to the menu.\n");

			this.pause();
			return;

		}

		if (optionText.length() > 70) 
		{

			System.out.print("\n===== Error: Option cannot exceed 70"
				+ " characters in length.\n"
				+ "The option\n"
				+ optionText + "\n"
				+ "was not added to the menu.\n");

			this.pause();
			return;

		}

		this.options.add(optionText);

	}

   /**
    Displays the menu title and options, with a blank line between the title 
    and the options. The menu is positioned more or less centered on the screen,
    with any "bias" toward the left and the top. If the menu currently has no 
    options, the title is displayed, then a blank line, and then this line is 
    displayed, starting at the left margin.
    <pre>
    This Menu currently has no options.
    </pre>
    These three lines are centered vertically, more or less, on an otherwise 
    blank screen.
    @pre.c This Menu object has been initialized.
    @post.c This Menu object has been displayed on the screen.
    */
	public void display() 
	{

		//center title
		String spacesBeforeTitle = this.generateSpaces((80 
			- this.title.length()) / 2);

		//v-center menu
		String linesBeforeAfter = this.generateLines((25 
			- (2 + this.options.size())) / 2);


		String spacesBeforeOption = "";
		int longestTitle = 0;
		if (this.options.size() > 0) {

			for (int i = 1; i < this.options.size(); i++) {

				if (this.options.get(i).length() > 
					this.options.get(longestTitle).length())
					longestTitle = i;

			}

			spacesBeforeOption = this.generateSpaces((80 
				- (this.options.get(longestTitle).length() 
				+ (this.options.size() > 10 ? 4 : 3))) / 2);
			System.out.println(this.options.get(longestTitle));

		}
 
		System.out.print(linesBeforeAfter);		

		if (this.options.size() <= 0) 
		{

			System.out.println(this.title + "\n");
			System.out.println("This menu currently has no options ... ");

		} else 
		{

			System.out.println(spacesBeforeTitle + this.title + "\n");

			boolean extraSpace = this.options.size() >= 10 ? true : false;

			for (int i = 0; i < this.options.size(); i++) {

				String optionNumber = (extraSpace ? (((i + 1) < 10) ? " "  : "")
				 : "") + (i + 1) + ". ";
				System.out.println(spacesBeforeOption + optionNumber
				 + this.options.get(i));

			}

		}

		System.out.print(linesBeforeAfter);	

	}

	//makes any number of spaces
	private String generateSpaces(int number) 
	{
		String spaces = "";
		for (int i = 1; i <= number; i++)
			spaces += " ";

		return spaces;
	}

   //makes any number of lines
	private String generateLines(int number) 
	{
		String lines = "";
		for (int i = 1; i <= number; i++)
			lines += "\n";

		return lines;
	}
	
	//makes a pause
	private void pause() 
	{

        System.out.print("Press Enter to continue ... ");
        this.kb.nextLine();

    }

}