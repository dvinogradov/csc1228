//OpeningScreen.java
//Vinogradov:Dimitry:A00358584:CSC122834
//Submission 05
//Constructors for the Opening Screen Class
/* - SELF ASSESSMENT - 
Program runs great, no errors on my end
*/

import java.util.Scanner;

public class OpeningScreen {

    private Scanner kb = new Scanner(System.in);
    private String message = "";

    /**
    Creates a default OpeningScreen object containing three lines of 
    text output that identify the programmer and the submission. The output 
    shown when this object is displayed is given on the following three lines:
    <pre>
    Lastname:Firstname:A00123456:csc122801
    Submission 05
    Constructors for the Opening Screen Class</pre>
    The default indentation is 16 spaces, and there are 11 blank lines 
    before and 11 blank lines after the above three lines of output. 
    <br />
    Pre: None. <br />
    Post: An OpeningScreen object has been constructed in which all attributes
    have default values.
    */
    public OpeningScreen()
    {

        this("Vinogradov:Dimitry:A00358584:csc122834",
             "Submission 05",
             "Constructors for the Opening Screen Class", 16, 11, 11);

    }

    /**
    Creates an OpeningScreen object with client-supplied three lines of text 
    output to be displayed. The default indentation is 16 spaces, and there 
    are 11 blank lines before and 11 blank lines after the three lines of 
    output.
    @param firstLine The first line of text in the display.
    @param secondLine The second line of text in the display.
    @param thirdLine The third line of text in the display.
    <br />
    Pre: All input parameters have been initialized. <br /> 
    Post: An OpeningScreen object has been constructed according to the 
    information provided by the actual values of the input parameters.
    */
    public OpeningScreen(String firstLine, 
                         String secondLine, 
                         String thirdLine) 
    {

        this(firstLine, secondLine, thirdLine, 16, 11, 11);

    }

    /**
    Creates an OpeningScreen object with client-supplied three lines of 
    text output to be displayed, as well as a client-supplied indentation
    level. There are 11 blank lines before and 11 blank lines after the three
    lines of output.
    @param firstLine The first line of text in the display.
    @param secondLine The second line of text in the display.
    @param thirdLine The third line of text in the display.
    @param numberOfSpacesToIndent - The number of spaces the lines of 
    information are indented from the left margin. 
    <br />
    Pre: All input parameters have been initialized. <br />
    Post: An OpeningScreen object has been constructed according to the 
    information provided by the actual values of the input parameters.
    */
    public OpeningScreen(String firstLine, 
                         String secondLine, 
                         String thirdLine, 
                         int numberOfSpacesToIndent) 
    {

        this(firstLine, 
            secondLine, 
            thirdLine, 
            numberOfSpacesToIndent, 11, 11);

    }


    /**
    Creates an OpeningScreen object with client-supplied three lines of text
    output to be displayed, as well as a client-supplied number of spaces to 
    indent, in addition to a client-supplied number of blank lines before and 
    after the three lines of output.
    @param firstLine The first line of text in the display.
    @param secondLine The second line of text in the display.
    @param thirdLine The third line of text in the display.
    @param numberOfSpacesToIndent - The number of spaces the lines of 
    information are indented from the left margin.
    @param numberOfBlankLinesBefore - The number of blank lines displayed 
    before the three lines of output.
    @param numberOfBlankLinesAfter - The number of blank lines displayed 
    after the three lines of output. 
    <br />
    Pre: All input parameters have been initialized. <br />
    Post: An OpeningScreen object has been constructed according to the
    information provided by the actual values of the input parameters.
    */
    public OpeningScreen(String firstLine,
                         String secondLine,
                         String thridLine,
                         int numberOfSpacesToIndent,
                         int numberOfBlankLinesBefore,
                         int numberOfBlankLinesAfter) 
    {

        String indents = "";

        //first new lines
        for (int i = 1; i <= numberOfBlankLinesBefore; i++)
            this.message += "\n";

        //indent size
        for (int i = 1; i <= numberOfSpacesToIndent; i++)
            indents += " ";

        //actual message
        this.message += indents + firstLine + "\n" +
                       indents + secondLine + "\n" + 
                       indents + thridLine;

        //last new lines
        for (int i = 0; i <= numberOfBlankLinesAfter; i++)
            this.message += "\n";

    }


    /**
    Displays the current OpeningScreen object. <br />
    Pre: This object has been initialized. <br />
    Post: Three lines of (typically, but not necessarily indented) text 
    information have been output, with (typically, but not necessarily) 
    some blank lines before and after that text, and with a pause at the end.
    */
    public void display() {

        System.out.print(this.message);
        this.pause();

    }

    /**
    "Pauses" the console <br />
    Pre: This object has been initialized. <br />
    Post: Prints out "Press Enter to continue ... " and simply waits for \n to 
    come about
    */
    private void pause() {

        System.out.print("Press Enter to continue ... ");
        this.kb.nextLine();

    }

}