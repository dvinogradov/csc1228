//--Change the name of the file in the comment line immediately below.
//DecimalBinaryConversion1.cpp
//--Edit the following line so that it contains your information.
//Vinogradov:Dimitry:A00358584:CSC122834
//Submission 08
//Decimal/Binary Conversion, Part 1 (C+version)

/*
 * Program runs great, just like sub01
 */

#include <iostream>
#include <string>
#include <ctype.h>

using std::cout;
using std::cin;
using std::endl;
using std::string;

bool isInteger(string);
string convertToBinary(int);


/**
 * This program converts non-negative decimal (ie, base 10) integers
 * to their corresponding non-negative binary (ie, base 2) integer
 * counterparts.
 */
int main()
{
    //Display identification information and pause
    cout << "\nVinogradov:Dimitry:A00358584:CSC122834\n"
            "Submission 01\n"
            "Decimal/Binary Conversion, Part 1\n"
            "Press Enter to continue ... ";
    cin.ignore(80, '\n');

    //Display program description and pause
    cout << "\nThis program allows the user to convert "
            "a decimal integer entered by the user\nto its equivalent "
            "binary value. It then displays, on the standard output, a "
            "\nsentence giving both the original decimal integer and "
            "the converted binary\nvalue. Note that \"decimal integer\" "
            "simply means a base 10 integer, and we deal\nonly with "
            "non-negative values."

            "\n\nDecimal values can have leading zero digits when input, "
            "but they will not be\ngiven any when output. Any decimal "
            "value input must be in the range 0..65535."
 
            "\n\nThe program can convert any number of values on any "
            "given run. On each pass\nthe user must either choose to "
            "quit (by entering q, or Q, and pressing Enter),\nor choose "
            "to convert another value (by simply pressing Enter). If "
            "the user\nenters anything else at this point, the program "
            "reports an error and allows\nthe user to try again."
           
            "\n\nWhen the conversion option is chosen, a valid decimal "
            "integer in the required\nrange is then expected as input. "
            "If the decimal value entered does not fall\nwithin the "
            "required range of values, an error must be reported and "
            "the value\nmust be ignored by the program, which simply "
            "carries on after reporting the\nerror. The program is not "
            "responsible for dealing with the kind of error that\noccurs "
            "if anything other than an integer value is entered "
            "when a valid non-\nnegative decimal integer is expected.\n\n"
            "Press Enter to continue ... ";
    cin.ignore(80, '\n');
     
    //Convert a value or terminate the program ...
     //check for exiting the loop or not
    bool didNotExit = true;

    do 
    {


        //prompt the user and get the value from kb
        cout << "\nEnter q (or Q) to exit or just press Enter to " 
                "convert a value: ";
        string valueFromKeyboard;
        getline(cin, valueFromKeyboard);
            
        //check if user wishes to exit
        if (valueFromKeyboard == "q" || valueFromKeyboard == "Q") 
        {
            
            cout << "\nQuit option chosen.\nProgram now"
                    " terminating.\nPress Enter to continue ... ";
           
            cin.ignore(80, '\n');
            return 0;

        } 
        else if (valueFromKeyboard.empty()) 
        {
            
            int newValueFromKeyboard;
            cout << "\nEnter a decimal integer in the "
                    "range 0..65535: ";
            cin >> newValueFromKeyboard;

            //check the range
            if (newValueFromKeyboard > 65535 || newValueFromKeyboard < 0) 
            {
                
                cout << "\nError: Decimal value out of"
                        " range.\nTry again.\n";
                        "Press Enter to continue ... ";
                cin.ignore(80, '\n');

            } 
            else
            {     
                //do the magic here                       
                cout << "The corresponding binary value "
                        "of decimal value " << newValueFromKeyboard << " is " 
                        << convertToBinary(newValueFromKeyboard) << ".";   
                cout << "\nPress Enter to continue ... ";    
                
                cin.ignore(80, '\n'); cin.ignore(80, '\n'); 
            }

            didNotExit = true;
                
        } 
        else 
        {
            cout << "\nError: Invalid response."
                    "\nTry again.\nPress Enter to continue ... ";
            
            cin.ignore(80, '\n');

            didNotExit = true;

        }

    } while(didNotExit);
        


    return 0;
}

string convertToBinary(int value) {
 
    string binaryVal = "";

    while (value != 0) 
    {
        if (value % 2 == 0)
            binaryVal = "0" + binaryVal;
        else
            binaryVal = "1" + binaryVal;

        value /= 2;
    }

    return binaryVal;
    
}