//-Change the name of the file in the comment line immediately below.
//DecimalBinaryConversion2.cpp
//--Edit the following line so that it contains your information.
//Lastname:Firstname:A00123456:csc122801
//Submission 09
//Decimal/Binary Conversion, Part 2 (C++ version)

/*
* works great, no problems
*/

//Put your #include directives and your using statement here.
#include <iostream>
#include <string>
#include <cmath>
#include <sstream>

using std::cout;
using std::cin;
using std::endl;
using std::stringstream;
using std::string;


//The following are the prototypes of the functions you must implement
void DisplayIdentificationInfo();
/**<
* Displays identification information and pauses for user to press Enter.
*/

void DisplayProgramDescription();
/**<
* Displays program description and pauses for user to press Enter.
*/

char getRequestedAction();
/**<
* Prompts the user to enter a single character to choose whether to
* convert a binary or decimal value, or to quit the program, and then
* returns that value. If the value entered is not one of the valid
* single characters, an error is reported and the user is asked to
* try again. [Run sample executable to see the required format for
* the prompt and error message.]
*/

string getBinaryValueFromUser();
/**<
* Prompts the user to enter a single character to choose whether to
* convert a binary or decimal value, or to quit the program, and then
* returns that value. If the value entered is not one of the valid
* single characters, an error is reported and the user is asked to
* try again. [Run sample executable to see the required format for
* the prompt and error message.]
*/

int decimalFromBinary
 (
 string binaryValue //in
 );
/**<
* Takes in a binary value as input and returns the
* corresponding decimal value.
*/

int getDecimalValueFromUser();
/**<
* Gets a decimal value from the user, entered from the keyboard.
* This decimal value must be valid, That is, it must contain
* only digits from the range 0..9, and as an integer value must
* lie in the range 0..65535. If it is not valid, the program
* outputs an error message and asks the user to try again.
* [Run sample executable to see the required format for the
* prompt and error message.] 
*/

string binaryFromDecimal
 (
 int decimalValue //in
 );
/**<
* Takes in a decimal value as input and returns the
* corresponding binary value.
*/
bool isInt(string);

/**
* This program converts non-negative decimal (ie, base 10) integers
* to their corresponding non-negative binary (ie, base 2) integer
* counterparts, or vice versa.
*/
int main()
{
    //Put here the code for your main() driver function.
    //It will have the same overall structure as its Java counterpart.
    DisplayIdentificationInfo();
    DisplayProgramDescription();
     
    bool finished = false;
     
    do {
       string binaryValue;
       int decimalValue;
       char requestedAction = getRequestedAction();
       switch (requestedAction)
       {
       case 'b': case 'B':
           binaryValue = getBinaryValueFromUser();
           decimalValue = decimalFromBinary(binaryValue);
           cout << "The corresponding decimal value of "
                   "binary value " << binaryValue << " is "
                   << decimalValue << "." << endl;
            cout << "Press Enter to continue ... ";
            cin.ignore(80, '\n');
            break;
        case 'd': case 'D':
            decimalValue = getDecimalValueFromUser();
            binaryValue = binaryFromDecimal(decimalValue);
            cout << "The corresponding binary value of "
                    "decimal value " << decimalValue << " is "
                << binaryValue << "." << endl;
            cout << "Press Enter to continue ... ";
            cin.ignore(80, '\n');
            break;
        case 'q': case 'Q':
            finished = true;
            cout << "\nQuit option chosen."
                    "\nProgram now terminating." << endl;
            cout << "Press Enter to continue ... ";
           cin.ignore(80, '\n');
            break;
        }
    } while (!finished);
}


//The following are the functions definitions corresponding to the
//function prototypes given above (before main()).
void DisplayIdentificationInfo()
{
    cout << "\nVinogradov:Dimitry:A00358584:csc122834" << endl;
    cout << "Submission 02" << endl;
    cout << "Decimal/Binary Conversion, Part 2 (C++ Version)" << endl;
    cout << "Press Enter to continue ... ";
    cin.ignore(80, '\n');
}


void DisplayProgramDescription()
{
    cout << "\nThis program allows the user to convert either "
            "a binary value entered by the\nuser to its equivalent decimal "
            "value, or a decimal value entered by the user\nto its "
            "equivalent binary value. In either case, it then displays "
            "both the\noriginal value and the converted value on the "
            "standard output."
            
            "\n\nThe program may convert any number of values of either "
            "type on any given run.\nOn each pass the user must either "
            "choose to quit, or choose what kind of value\nhe or she "
            "wishes to convert. If a conversion option is chosen, then "
            "a valid\nvalue of the correct type is the expected input. "
            "The program deals only with\nnon-negative values."
            
            "\n\nA binary value is valid (in our case) if it contains "
            "sixteen or fewer binary\ndigits. A binary digit is a 0 or a 1."
            
            "\n\nA decimal value is valid (in our case) if it contains only "
            "digits in the range\n0..9, and its value lies in the range "
            "0..65535. Note that we do not permit our\ndecimal integer "
            "values to be preceded by a + sign."
            
            "\n\nIf a value of either type does not satisfy the necessary "
            "criteria, an error\nmust be reported and the value must be "
            "ignored by the program, which simply\nasks for another value "
            "after reporting the error. In addition, if the user\ndoes not "
            "respond correctly when asked to choose the kind of value to "
            "convert,\nat that point the program also reports the error "
            "and repeats the request." << endl;
            
    cout << "Press Enter to continue ... ";
    cin.ignore(80, '\n');
}


char getRequestedAction()
{
      
    cout << "\nWhat kind of value would you like to convert?" << endl;
    cout << "Enter b (or B) for binary, d (or D) for decimal, "
            "or q (or Q) to quit: ";
    
    string userAction;
    getline(cin, userAction);
    
    if (userAction.length() > 1 || userAction.empty()) 
    {

        cout << "\nError: Invalid response.\n"
                "Try again." << endl;
        cout << "Press Enter to continue ... ";
        cin.ignore(80, '\n');

        //recursive yo
        return getRequestedAction();

    } else   
      return userAction[0];
}


string getBinaryValueFromUser()
{
    
    string binaryValue = "";
    
    cout << "\nEnter a binary value containing up to 16 digits: ";
    
    getline(cin, binaryValue);
    
    if (binaryValue.length() <= 16) {
                                 
        for (int i = 0; i <= binaryValue.length() - 1; i++) {   
        
            if (!(binaryValue[i] == '1' ||
                binaryValue[i] == '0'))
            {
                cout << "\nError: Invalid binary value.\n"
                        "Try again." << endl;
                cout << "Press Enter to continue ... ";
                cin.ignore(80, '\n');
        
                return getBinaryValueFromUser();
            }      
        }
        
    } else {
        cout << "\nError: Invalid binary value.\n"
                "Try again." << endl;
        cout << "Press Enter to continue ... ";
        cin.ignore(80, '\n');
        
        return getBinaryValueFromUser();
    }
    
    return binaryValue;
}


int decimalFromBinary
 (
 string binaryValue //in
 )
{
    int decimalValue = 0;
    
    for (int i = binaryValue.length() - 1, j = 0; i >= 0; i--,j++) {
        if (binaryValue[i] != '0')
            decimalValue += (int)pow(2.0, j);
    }
    
    return (int)decimalValue;
}


int getDecimalValueFromUser()
{
    
    cout << "\nEnter a decimal integer in the "
            "range 0..65535: ";
    
    int originalNumber = 0;
    string newValueFromKeyboard;
    getline(cin, newValueFromKeyboard);
    
    if (isInt(newValueFromKeyboard)) 
    {
        stringstream(newValueFromKeyboard) >> originalNumber;
        
        if (originalNumber > 65535 || originalNumber < 0)
        {
    
            cout << "\nError: Invalid decimal value."
                    "\nTry again.\nPress Enter to continue ... ";
            cin.ignore(80, '\n');
    
            //recurse
            return getDecimalValueFromUser();
    
        }            
    } 
    else 
    {
    
        cout << "\nError: Invalid decimal value."
                "\nTry again.\nPress Enter to continue ... ";
        cin.ignore(80, '\n');
    
        //recurse
        return getDecimalValueFromUser();
    
    }
    
    return originalNumber;
}


string binaryFromDecimal
 (
 int decimalValue //in
 )
{
    string binaryVal = "";
    
    while (decimalValue != 0) 
    {
        if (decimalValue % 2 == 0)
            binaryVal = "0" + binaryVal;
        else
            binaryVal = "1" + binaryVal;
    
        decimalValue /= 2;
    }
    
    return binaryVal;
}

bool isInt(string value) {
     int i;
     return (stringstream(value) >> i) ? true : false;     
}
