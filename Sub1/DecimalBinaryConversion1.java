//DecimalBinaryConversion1.java
//--Modify the following line to contain your own information.
//Vinogradov:Dimitry:A00358584:CSC122834
//Submission 01
//Decimal/Binary Conversion, Part 1

/*
 * Your program self-assessment goes here, as a courtesy to the marker.
 */

import java.util.Scanner;

/**
 * This class contains a complete program, with just a main() method,
 * for converting non-negative decimal integers (ie, base 10 integers) to
 * non-negative binary integers (ie, base 2 integers). The user may perform
 * as many conversions as desired during a single run of the program.
 */
public class DecimalBinaryConversion1
{
    public static void main(String[] args)
    {
        Scanner keyboard = new Scanner(System.in);

        //Display identification information and pause
        //--Modify the following line to contain your own information.
        System.out.println("\nVinogradov:Dimitry:A00358584:CSC122834");
        System.out.println("Submission 01");
        System.out.println("Decimal/Binary Conversion, Part 1");
        System.out.print("Press Enter to continue ... ");
        keyboard.nextLine();

        //Display program description and pause
        System.out.println("\nThis program allows the user to convert "
            + "a decimal integer entered by the user\nto its equivalent "
            + "binary value. It then displays, on the standard output, a "
            + "\nsentence giving both the original decimal integer and "
            + "the converted binary\nvalue. Note that \"decimal integer\" "
            + "simply means a base 10 integer, and we deal\nonly with "
            + "non-negative values."

            + "\n\nDecimal values can have leading zero digits when input, "
            + "but they will not be\ngiven any when output. Any decimal "
            + "value input must be in the range 0..65535."
 
            + "\n\nThe program can convert any number of values on any "
            + "given run. On each pass\nthe user must either choose to "
            + "quit (by entering q, or Q, and pressing Enter),\nor choose "
            + "to convert another value (by simply pressing Enter). If "
            + "the user\nenters anything else at this point, the program "
            + "reports an error and allows\nthe user to try again."
           
            + "\n\nWhen the conversion option is chosen, a valid decimal "
            + "integer in the required\nrange is then expected as input. "
            + "If the decimal value entered does not fall\nwithin the "
            + "required range of values, an error must be reported and "
            + "the value\nmust be ignored by the program, which simply "
            + "carries on after reporting the\nerror. The program is not "
            + "responsible for dealing with the kind of error that\noccurs "
            + "if anything other than an integer value is entered "
            + "when a valid non-\nnegative decimal integer is expected.\n");
        System.out.print("Press Enter to continue ... ");
        keyboard.nextLine();
        
        //--Complete the program by entering here the necessary Java code
        //--so that your program performs exactly like the sample executable.
        
        //check for exiting the loop or not
        boolean didNotExit = true;

        do 
        {
            //prompt the user and get the value from kb
            System.out.print("\nEnter q (or Q) to exit or just press Enter to " 
                + "convert a value: ");
            String valueFromKeyboard = keyboard.nextLine();
            
            //check if user wishes to exit
            if (valueFromKeyboard.toLowerCase().equals("q")) 
            {
                System.out.print("\nQuit option chosen.\nProgram now"
                + " terminating.\nPress Enter to continue ... ");
                keyboard.nextLine();
                System.exit(0);
            } 
            else if (valueFromKeyboard.equals("")) 
            {

                System.out.print("\nEnter a decimal integer in the "
                    + "range 0..65535: ");
                
                /*The right way, handles the non-integer error
                String newValueFromKeyboard = keyboard.nextLine();


                if (isInt(newValueFromKeyboard)) {
                    int originalNumber = Integer.parseInt(newValueFromKeyboard);
                    
                    if (originalNumber > 65535 || originalNumber < 0) {
                        System.out.println("\nError: Decimal value out of"
                        + " range.\nTry again.");
                        System.out.print("Press Enter to continue ... ");
                        keyboard.nextLine();
                    } else {                            
                        System.out.println("The corresponding binary value "
                            + "of decimal value " + originalNumber + " is " 
                            + convertToBinary(originalNumber) + ".");   
                        System.out.print("Press Enter to continue ... ");    
                        keyboard.nextLine();  
                    }
      
                } else {
                    System.out.print("\nError: Invalid response."
                        + "\nTry again.\nPress Enter to continue ... ");
                    keyboard.nextLine();
                }*/

                //the correct way, doesn't handle the non-integer error
                int newValueFromKeyboard = keyboard.nextInt();
                keyboard.nextLine();

                //check the range
                if (newValueFromKeyboard > 65535 || newValueFromKeyboard < 0) 
                {
                    System.out.println("\nError: Decimal value out of"
                    + " range.\nTry again.");
                    System.out.print("Press Enter to continue ... ");
                    keyboard.nextLine();
                } 
                else
                {     
                    //do the magic here                       
                    System.out.println("The corresponding binary value "
                        + "of decimal value " + newValueFromKeyboard + " is " 
                        + convertToBinary(newValueFromKeyboard) + ".");   
                    System.out.print("Press Enter to continue ... ");    
                    keyboard.nextLine();  
                }

                didNotExit = true;
                
            } 
            else 
            {
                System.out.print("\nError: Invalid response."
                    + "\nTry again.\nPress Enter to continue ... ");
                keyboard.nextLine();
                didNotExit = true;
            }
        } while(didNotExit);
        
    }

    //checks if string is an int
    public static boolean isInt(String in)
    {
        try 
        {
            Integer.parseInt(in);
            return true;
        } 
        catch (Exception e) 
        {
            return false;
        }
    }

    //converts int to binary
    public static String convertToBinary(int value) 
    {
        String binaryVal = "";

        while (value != 0) 
        {
            if (value % 2 == 0)
                binaryVal = "0" + binaryVal;
            else
                binaryVal = "1" + binaryVal;

            value /= 2;
        }

        return binaryVal;
    }
}